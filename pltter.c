#if 0

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <conio.h>
#include <commander.h>
#include <image.h>
#include <disc.h>

#include "stretchy_buffer.h"

static uint32_t colors[] = {
	// Colors 0 to 15: original ANSI colors
	0x000000, 0xcd0000, 0x00cd00, 0xcdcd00, 0x0000ee, 0xcd00cd, 0x00cdcd, 0xe5e5e5,
	0x7f7f7f, 0xff0000, 0x00ff00, 0xffff00, 0x5c5cff, 0xff00ff, 0x00ffff, 0xffffff,
	// Color cube colors
	0x000000, 0x00005f, 0x000087, 0x0000af, 0x0000d7, 0x0000ff, 0x005f00, 0x005f5f,
	0x005f87, 0x005faf, 0x005fd7, 0x005fff, 0x008700, 0x00875f, 0x008787, 0x0087af,
	0x0087d7, 0x0087ff, 0x00af00, 0x00af5f, 0x00af87, 0x00afaf, 0x00afd7, 0x00afff,
	0x00d700, 0x00d75f, 0x00d787, 0x00d7af, 0x00d7d7, 0x00d7ff, 0x00ff00, 0x00ff5f,
	0x00ff87, 0x00ffaf, 0x00ffd7, 0x00ffff, 0x5f0000, 0x5f005f, 0x5f0087, 0x5f00af,
	0x5f00d7, 0x5f00ff, 0x5f5f00, 0x5f5f5f, 0x5f5f87, 0x5f5faf, 0x5f5fd7, 0x5f5fff,
	0x5f8700, 0x5f875f, 0x5f8787, 0x5f87af, 0x5f87d7, 0x5f87ff, 0x5faf00, 0x5faf5f,
	0x5faf87, 0x5fafaf, 0x5fafd7, 0x5fafff, 0x5fd700, 0x5fd75f, 0x5fd787, 0x5fd7af,
	0x5fd7d7, 0x5fd7ff, 0x5fff00, 0x5fff5f, 0x5fff87, 0x5fffaf, 0x5fffd7, 0x5fffff,
	0x870000, 0x87005f, 0x870087, 0x8700af, 0x8700d7, 0x8700ff, 0x875f00, 0x875f5f,
	0x875f87, 0x875faf, 0x875fd7, 0x875fff, 0x878700, 0x87875f, 0x878787, 0x8787af,
	0x8787d7, 0x8787ff, 0x87af00, 0x87af5f, 0x87af87, 0x87afaf, 0x87afd7, 0x87afff,
	0x87d700, 0x87d75f, 0x87d787, 0x87d7af, 0x87d7d7, 0x87d7ff, 0x87ff00, 0x87ff5f,
	0x87ff87, 0x87ffaf, 0x87ffd7, 0x87ffff, 0xaf0000, 0xaf005f, 0xaf0087, 0xaf00af,
	0xaf00d7, 0xaf00ff, 0xaf5f00, 0xaf5f5f, 0xaf5f87, 0xaf5faf, 0xaf5fd7, 0xaf5fff,
	0xaf8700, 0xaf875f, 0xaf8787, 0xaf87af, 0xaf87d7, 0xaf87ff, 0xafaf00, 0xafaf5f,
	0xafaf87, 0xafafaf, 0xafafd7, 0xafafff, 0xafd700, 0xafd75f, 0xafd787, 0xafd7af,
	0xafd7d7, 0xafd7ff, 0xafff00, 0xafff5f, 0xafff87, 0xafffaf, 0xafffd7, 0xafffff,
	0xd70000, 0xd7005f, 0xd70087, 0xd700af, 0xd700d7, 0xd700ff, 0xd75f00, 0xd75f5f,
	0xd75f87, 0xd75faf, 0xd75fd7, 0xd75fff, 0xd78700, 0xd7875f, 0xd78787, 0xd787af,
	0xd787d7, 0xd787ff, 0xd7af00, 0xd7af5f, 0xd7af87, 0xd7afaf, 0xd7afd7, 0xd7afff,
	0xd7d700, 0xd7d75f, 0xd7d787, 0xd7d7af, 0xd7d7d7, 0xd7d7ff, 0xd7ff00, 0xd7ff5f,
	0xd7ff87, 0xd7ffaf, 0xd7ffd7, 0xd7ffff, 0xff0000, 0xff005f, 0xff0087, 0xff00af,
	0xff00d7, 0xff00ff, 0xff5f00, 0xff5f5f, 0xff5f87, 0xff5faf, 0xff5fd7, 0xff5fff,
	0xff8700, 0xff875f, 0xff8787, 0xff87af, 0xff87d7, 0xff87ff, 0xffaf00, 0xffaf5f,
	0xffaf87, 0xffafaf, 0xffafd7, 0xffafff, 0xffd700, 0xffd75f, 0xffd787, 0xffd7af,
	0xffd7d7, 0xffd7ff, 0xffff00, 0xffff5f, 0xffff87, 0xffffaf, 0xffffd7, 0xffffff,
	// >= 233: Grey ramp
	0x000000, 0x121212, 0x1c1c1c, 0x262626, 0x303030, 0x3a3a3a, 0x444444, 0x4e4e4e,
	0x585858, 0x626262, 0x6c6c6c, 0x767676, 0x808080, 0x8a8a8a, 0x949494, 0x9e9e9e,
	0xa8a8a8, 0xb2b2b2, 0xbcbcbc, 0xc6c6c6, 0xd0d0d0, 0xdadada,
};

// Find an xterm color value that matches an ARGB color
uint8_t rgb2xterm(uint8_t _r,uint8_t _g,uint8_t _b)
{
	uint8_t c, match = 0;
	uint32_t r, g, b, d, distance;
	distance = 1000000000;
	for(c = 0; c <= 253; c++) {
		r = ((0xff0000 & colors[c]) >> 16) - _r;
		g = ((0x00ff00 & colors[c]) >> 8)  - _g;
		b = (0x0000ff & colors[c]) - _b;
		d = r * r + g * g + b * b;
		if (d < distance) {
			distance = d;
			match = c;
		}
	}

	return match;
}

void plot(uint8_t x,uint8_t y,uint8_t r,uint8_t g,uint8_t b)
{
	x=x%200;
	y=y%50;
	printf("\x1b[38;2;%d;%d;%dm", r, g, b);
	printf("\033[%d;%dH",y+1,x);
//printf("\x1b[38;2;%d;%d;%dm", r, g, b);
//	uint8_t col2=rgb2xterm(r,g,b);
	//printf("\x1b[38;5;%dm", col2);	
	printf("%c", 219); 
}

//Copyright (c) 2019 MonstersGoBoom
// tilemizer main 

/*	tileformats 
	"genesis" 4bpp 
	"gba"			4bpp 
	"nes"			2bpp
	"sms"			4bpp ( gamegear , wonderswan color )
	"gb"			2bpp
	"snes3"		3bpp
	"snes"		4bpp
	"pce"			4bpp
	"vboy"		2bpp
	"ngpc"		2bpp
*/

//	map formats aren't supported yet 
//	just raw 16 bit if defined 
//#define OUT_MAP

#define TILE_W 8 
#define TILE_H 8 

typedef struct 
{
	uint32_t 	crc;
	uint8_t 	clut;	//	index 
	uint8_t 	bytes[TILE_W*TILE_H];
} TILE;

typedef struct 
{
	uint32_t 	crc;
	uint16_t 	data[8*8];
} META_TILE;

FILE *tiles_fp;
FILE *map_fp;
FILE *meta_fp;
FILE *clut_fp;

static struct 
{
	uint8_t planes;
	uint8_t repeats_x:1;
	uint8_t repeats_y:1;
	uint8_t repeats:1;
	uint8_t saveMap:2;	//	0 no ,1 8 bit , 2 16 bit , 3 spare
	uint8_t asCoords:1;
	uint8_t meta_tile_x;
	uint8_t meta_tile_y;
	uint16_t map_w;
	uint16_t map_h;
	uint8_t bpp;
	uint8_t max_colors;
	image_t bitmap;
	uint16_t *map;
	TILE **tiles;
	META_TILE **metatiles;
} OPTS;

static void outname(command_t *self)
{
  printf("Output: %s\n", self->arg);
}

static void out_metatile_size(command_t *self)
{
char *isvalid = strchr(self->arg,'*');
	if (isvalid==NULL)
	{
		printf("format isn't X*Y\n");
		exit(0);
		return;
	}
	char *xarg=(char*)&self->arg[0];
	char *yarg=isvalid+1;
	*isvalid = 0;

	OPTS.meta_tile_x = atoi(xarg);
	OPTS.meta_tile_y = atoi(yarg);
	if ((OPTS.meta_tile_x<1) || (OPTS.meta_tile_y<1))
	{
		OPTS.meta_tile_x=0;
		OPTS.meta_tile_y=0;
		return;
	}
	if (OPTS.meta_tile_x>8) OPTS.meta_tile_x=8;
	if (OPTS.meta_tile_y>8) OPTS.meta_tile_y=8;
	printf("Meta Tiles %d by %d\n",OPTS.meta_tile_x,OPTS.meta_tile_y);
}

//	command line opt -m b <bytes> -m w <words>
static void out_map(command_t *self)
{
	if (self->arg==NULL)
		OPTS.saveMap = 2;
	else 
	{
		if (strchr(self->arg,'b')!=NULL)
			OPTS.saveMap = 1;
		if (strchr(self->arg,'w')!=NULL)
			OPTS.saveMap = 2;
	}
}

//	command line opt -r xy select axis repeat checking 

static void check_repeats(command_t *self)
{
	OPTS.repeats=1;
	if (self->arg!=NULL)
	{
		OPTS.repeats_x=strchr(self->arg,'x')!=NULL;
		OPTS.repeats_y=strchr(self->arg,'y')!=NULL;
	}
}

//	command line opt -p 1,2,3,4 only supports up to 4 planes as output 

static void set_plane_format(command_t *self)
{
	uint8_t p = atoi(self->arg);
	if (p<1) p=1;
	if (p>4) p=4;
	OPTS.planes=p;
	printf("Planes set %d\n",OPTS.planes);
}

//	command line opt -b 1,2,3,4 bitplanes for input data 
//	say you have an image where your palettes
//  4 colors each ( NES ) image is Clut 0 0123 Clut 1 4567 etc. 
//  8 colors each ( ? ) image is Clut 0 01234567 Clut 1 89ABCDEF etc. 
//  16 colors each ( Genesis ) image is Clut 0 00010203-0F Clut 1 10111213-1F etc.  

static void set_max_colors(command_t *self)
{
	uint8_t p = atoi(self->arg);
	if (p<1) p=1;
	if (p>8) p=4;
	OPTS.bpp=p;
	OPTS.max_colors = 1<<(p);

	printf("BPP set %d %d\n",OPTS.bpp,OPTS.max_colors);
}

static void tile_show(TILE *tile,int xo,int yo)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t byte=0;
		for (int x=0;x<TILE_W;x++)
		{
			byte = tile->bytes[(_y*TILE_W)+x];
			uint8_t r=OPTS.bitmap.colors[(byte*4)+2];
			uint8_t g=OPTS.bitmap.colors[(byte*4)+1];
			uint8_t b=OPTS.bitmap.colors[(byte*4)+0];
			plot(xo+x,yo+_y,r,g,b);
		}
	}
	printf("\n\x1b[38;2;%d;%d;%dm", 255,255,255);
}

static void image_show()
{
	uint16_t mtx = OPTS.meta_tile_x*TILE_H;	
	uint16_t mty = OPTS.meta_tile_y*TILE_W;	
	if (mtx==0) mtx=TILE_W;
	if (mty==0) mty=TILE_H;

	printf("\e[1;1H\e[2J");
/*	
	for (uint16_t _y=0;_y<OPTS.bitmap.height/mty;_y++)
	{
		uint8_t *ptr=&OPTS.bitmap.buffer[((_y*mty)*OPTS.bitmap.width)];
		uint8_t byte=0;

		for (unsigned int x=0;x<OPTS.bitmap.width/mtx;x++)
		{
			byte = ptr[0];
			ptr+=mtx;
			uint8_t r=OPTS.bitmap.colors[(byte*4)+2];
			uint8_t g=OPTS.bitmap.colors[(byte*4)+1];
			uint8_t b=OPTS.bitmap.colors[(byte*4)+0];
			plot(x,_y,r,g,b);
		}
	}
*/
	for (uint16_t _y=0;_y<50;_y++)
	{
		for (unsigned int x=0;x<200;x++)
		{
			plot(x,_y,x,_y*3,0);
		}
	}


}

static void clut_show()
{
	for (uint16_t _y=0;_y<256;_y++)
	{
		uint8_t byte = _y;
		uint8_t r=OPTS.bitmap.colors[(byte*4)+2];
		uint8_t g=OPTS.bitmap.colors[(byte*4)+1];
		uint8_t b=OPTS.bitmap.colors[(byte*4)+0];
		plot(_y%100,_y/100,r,g,b);
	}
}


//	machine specific tile saves 
//	virtual boy 
static void tile_save_vboy(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>2;x++)
		{
			byte=(*ptr++&0x3);
			byte|=(*ptr++&0x3)<<2;
			byte|=(*ptr++&0x3)<<4;
			byte|=(*ptr++&0x3)<<6;
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}
//	machine specific tile saves 
//	neo geo pocket color
static void tile_save_ngpc(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)+TILE_W-1];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>2;x++)
		{
			byte=(*ptr--&0x3);
			byte|=(*ptr--&0x3)<<2;
			byte|=(*ptr--&0x3)<<4;
			byte|=(*ptr--&0x3)<<6;
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}
//	machine specific tile saves 
//	Sega Genesis 
static void tile_save_genesis(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>1;x++)
		{
			byte=(*ptr++&0xf)<<4;
			byte|=(*ptr++&0xf);
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}
//	machine specific tile saves 
//	Gameboy Advance
static void tile_save_gba(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>1;x++)
		{
			byte=(*ptr++&0xf);
			byte|=(*ptr++&0xf)<<4;
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}

//	machine specific tile saves 
//	SNES , PC Engine
static void tile_save_snes_planes(TILE *tile)
{
	for (int y=0;y<TILE_H;y++)
	{
		for (int plane=0;plane<2;plane++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
	for (int y=0;y<TILE_H;y++)
	{
		for (int plane=2;plane<OPTS.planes;plane++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}

//	none specific tile saves 
//	as bitplanes 
static void tile_save_planes(TILE *tile)
{
	for (int plane=0;plane<OPTS.planes;plane++)
	{
		for (int y=0;y<TILE_H;y++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}

//	none specific tile saves 
//	as interleaved bitplanes 
static void tile_save_interleaved_planes(TILE *tile)
{
	for (int y=0;y<TILE_H;y++)
	{
		for (int plane=0;plane<OPTS.planes;plane++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}

static void clut_save_RGB888(image_t *image)
{
	for (int q=0;q<256;q++)
	{
		uint8_t r=image->colors[(q*4)+2];
		uint8_t g=image->colors[(q*4)+1];
		uint8_t b=image->colors[(q*4)+0];
		fwrite(&r,1,1,clut_fp);
		fwrite(&g,1,1,clut_fp);
		fwrite(&b,1,1,clut_fp);
	}
}

static void clut_save_Genesis(image_t *image)
{
	for (int q=0;q<256;q++)
	{
		uint16_t rgb;
		uint8_t r=image->colors[(q*4)+2];
		uint8_t g=image->colors[(q*4)+2];
		uint8_t b=image->colors[(q*4)+2];
		rgb= (r>>3)<<10;
		rgb|=(g>>3)<<5;
		rgb|=(b>>3);
		fwrite(&rgb,2,1,clut_fp);
	}
}


static void (*tile_save)(TILE *tile)=tile_save_interleaved_planes;
static void (*clut_save)(image_t *image)=clut_save_RGB888;

typedef struct 
{
	const char *name;
	void ( *save)(TILE *tile); 
	void ( *saveclut)(image_t *tile); 
	uint8_t planes;
} TYPE_T;

TYPE_T types[]=
{
	{"genesis",tile_save_genesis,clut_save_Genesis,4},
	{"gba",tile_save_gba,clut_save_RGB888,4},
	{"nes",tile_save_planes,clut_save_RGB888,2},
	{"sms",tile_save_interleaved_planes,clut_save_RGB888,4},
	{"gb",tile_save_interleaved_planes,clut_save_RGB888,2},
	{"snes3",tile_save_snes_planes,clut_save_RGB888,3},
	{"snes",tile_save_snes_planes,clut_save_RGB888,4},
	{"pce",tile_save_snes_planes,clut_save_RGB888,4},
	{"vboy",tile_save_vboy,clut_save_RGB888,2},
	{"ngpc",tile_save_ngpc,clut_save_RGB888,2},
	{NULL,NULL,NULL,0},
};

static void set_machine(command_t *self)
{
	for (int q=0;types[q].name!=NULL;q++)
	{
		if (stricmp(self->arg,types[q].name)==0)
		{
			printf("%s\n",types[q].name);
			tile_save = types[q].save;
			clut_save = types[q].saveclut;
			OPTS.planes = types[q].planes;
		}
	}
}

static uint16_t tile_grab(int x,int y)
{
TILE b;
	b.crc = 0;
	b.clut = 0;

	for (int _y=0;_y<TILE_H;_y++)
	{
		for (int _x=0;_x<TILE_W;_x++)
		{
			uint8_t byte=OPTS.bitmap.buffer[(x+_x) + (((OPTS.bitmap.height-1)-(y+_y))*OPTS.bitmap.width)];	//	only bottom 4 bits 
			uint8_t clut= (byte/OPTS.max_colors)&0xf;
			if (((byte%OPTS.max_colors)!=0) && (b.clut==0))
				b.clut=clut;
			b.crc^=byte%OPTS.max_colors;
			b.bytes[_x+(_y*TILE_W)]=byte%OPTS.max_colors;
		}
	}
	uint16_t ret=sb_count(OPTS.tiles);
	
	if ((OPTS.repeats_x==1) || (OPTS.repeats_y==1) || (OPTS.repeats==1))
	{
		for (uint16_t q=0;q<ret;q++)
		{
			//	good chance of match
			if (OPTS.tiles[q]->crc==b.crc)
			{
				int mdiffer = 0;

				//	check fully to be sure 
				for (int _x=0;_x<TILE_W*TILE_H;_x++)
					if (OPTS.tiles[q]->bytes[_x]!=b.bytes[_x])
						mdiffer++;

				//	nope it's exactly the same , but the clut could be different 
				if (mdiffer==0)			
				{
					uint16_t ret=q&0x7ff;
					ret|=b.clut<<12;
					return ret;
				}	
				else 
				{
					//	check xflip
					if (OPTS.repeats_x==1)
					{
						mdiffer = 0;
						for (int _y=0;_y<TILE_H;_y++)
							for (int _x=0;_x<TILE_W;_x++)
								if (OPTS.tiles[q]->bytes[_x+(_y*TILE_W)]!=b.bytes[(TILE_W-1-_x)+(_y*TILE_W)])
									mdiffer++;

						if (mdiffer==0)			
						{
							uint16_t ret=q&0x7ff;
							ret|=b.clut<<12;
							ret|=1<<10;
							return ret;
						}
					}

					if (OPTS.repeats_x==1)
					{
						//	check yflip
						mdiffer = 0;
						for (int _y=0;_y<TILE_H;_y++)
							for (int _x=0;_x<TILE_W;_x++)
								if (OPTS.tiles[q]->bytes[_x+(_y*TILE_W)]!=b.bytes[_x+((TILE_H-1-_y)*TILE_W)])
									mdiffer++;

						if (mdiffer==0)			
						{
							uint16_t ret=q&0x7ff;
							ret|=b.clut<<12;
							ret|=1<<11;
							return ret;
						}

						if (OPTS.repeats_x==1)
						{
						//	check yflip & xflip
							mdiffer = 0;
							for (int _y=0;_y<TILE_H;_y++)
								for (int _x=0;_x<TILE_W;_x++)
									if (OPTS.tiles[q]->bytes[_x+(_y*TILE_W)]!=b.bytes[(TILE_W-1-_x)+((TILE_H-1-_y)*TILE_W)])
										mdiffer++;

							if (mdiffer==0)			
							{
								uint16_t ret=q&0x7ff;
								ret|=b.clut<<12;
								ret|=1<<10;
								ret|=1<<11;
								return ret;
							}
						}
					}
				}
			}
		}
	}
	tile_show(&b,(ret%25)*TILE_W,(ret/25)*TILE_H);

	TILE *copy = malloc(sizeof(TILE));
	memcpy(copy,&b,sizeof(TILE));
	sb_push(OPTS.tiles,copy);
	ret&=0x7ff;
	ret|=b.clut<<12;
	return ret;
}


static uint16_t metatile_grab(int x,int y)
{
	uint16_t ret;
	META_TILE mt;
	mt.crc = 0;


	for (int _y=0;_y<OPTS.meta_tile_y;_y++)
	{
		for (int _x=0;_x<OPTS.meta_tile_x;_x++)
		{
			uint16_t t=tile_grab((x+_x)*TILE_W,(y+_y)*TILE_H);
			mt.crc^=t%OPTS.max_colors;
			mt.data[_x+(_y*OPTS.meta_tile_x)]=t;
		}
	}

	//	we have a tile so check for repeats 
	ret=sb_count(OPTS.metatiles);
	for (uint16_t q=0;q<ret;q++)
	{
		//	good chance of match
		if (OPTS.metatiles[q]->crc==mt.crc)
		{
			int mdiffer = 0;
			//	check fully to be sure 
			for (int _x=0;_x<OPTS.meta_tile_x*OPTS.meta_tile_y;_x++)
				if (OPTS.metatiles[q]->data[_x]!=mt.data[_x])
					mdiffer++;
			if (mdiffer==0)			
			{
				ret=q;
				return ret;
			}	
		}
	}
	META_TILE *copy = malloc(sizeof(META_TILE));
	memcpy(copy,&mt,sizeof(META_TILE));
	sb_push(OPTS.metatiles,copy);
	return ret;
}


int main(int argc, char **argv)
{
  command_t cmd;

	OPTS.planes = 0;
	OPTS.repeats = 1;
	OPTS.repeats_x = 0;
	OPTS.repeats_y = 0;
	OPTS.saveMap = 0;
	OPTS.asCoords = 0;
	OPTS.tiles = NULL;
	OPTS.metatiles = NULL;
	OPTS.max_colors = 16;
	OPTS.bpp = 4;
	OPTS.meta_tile_x = 0;
	OPTS.meta_tile_y = 0;

  command_init(&cmd, argv[0], "0.1.0");
  command_option(&cmd, "-o", "--Output <arg>", "Output filename", outname);
	command_option(&cmd, "-p", "--Planes <arg>", "Number Of Planes -p 1-4", set_plane_format);
	command_option(&cmd, "-r", "--Repeats [arg]", "Check for repeats (-r xy) (-r x) (-r y)", check_repeats);

	command_option(&cmd, "-m", "--Meta <args>", "Output Map [ XxY b=byte,w=word ]", out_map);
	command_option(&cmd, "-t", "--metaTile <args>", "8x8 metatile size", out_metatile_size);
//	command_option(&cmd, "-c", "--Coordinates", "as xpos,ypos,tileid", set_outputmode);
	command_option(&cmd, "-b", "--Bits Per Pixel per tile <arg>", "#max 1,2,3,4", set_max_colors);
	command_option(&cmd, "-f", "--machine format <arg>", "genesis,nes,gba,ngpc,vb,snes4,snes3,pce", set_machine);

  command_parse(&cmd, argc, argv);

	printf("X repeats %d:",OPTS.repeats_x);
	printf("Y repeats %d\n",OPTS.repeats_y);
	printf("Map Output %d\n",OPTS.saveMap);
	printf("Colors Per Tile %d\n",OPTS.max_colors);
	printf("TileSize %dx%d\n",TILE_W,TILE_H);
	printf("MetaTile %dx%d\n",OPTS.meta_tile_x,OPTS.meta_tile_y);

	map_fp = fopen("_map.bin","wb");
	meta_fp = fopen("_meta.bin","wb");
	tiles_fp = fopen("_tiles.bin","wb");
	clut_fp = fopen("_clut.pal","wb");
  printf("additional args:\n");
  for (int i = 0; i < cmd.argc; ++i)
	{
		char *isbmp = strstr(cmd.argv[i],".bmp");
		if (isbmp!=NULL)
		{
			loadimage(cmd.argv[i],&OPTS.bitmap);
//			image_show();
			clut_show();
			OPTS.map_w = OPTS.bitmap.width / TILE_W;
			OPTS.map_h = OPTS.bitmap.height / TILE_H;
			printf("Map %dx%d\n",OPTS.map_w,OPTS.map_h);
			//	we always make a 16 bit map 
			//	how we output is later 
			if ((OPTS.meta_tile_x==0) || (OPTS.meta_tile_y==0))
			{
				OPTS.map = (uint16_t*)malloc(OPTS.map_w*OPTS.map_h*sizeof(uint16_t));
				for (int y=0;y<OPTS.map_h;y++)
				{
					for (int x=0;x<OPTS.map_w;x++)
					{
						OPTS.map[x+(y*OPTS.map_w)]=tile_grab(x*TILE_W,y*TILE_H);
					}
				}
			}
			else
			{
				uint16_t mw,mh;
				mw = OPTS.map_w/OPTS.meta_tile_x;
				mh = OPTS.map_h/OPTS.meta_tile_y;
				OPTS.map = (uint16_t*)malloc(mw*mh*sizeof(uint16_t));
				printf("Map %dx%d\n",mw,mh);

				//	map in segments
				for (int y=0;y<mh;y++)
				{
					for (int x=0;x<mw;x++)
					{
						OPTS.map[x+(y*mw)]=metatile_grab(x*OPTS.meta_tile_x,y*OPTS.meta_tile_y);
					}
				}
				OPTS.map_w = mw;
				OPTS.map_h = mh;
			}
		}
    printf("  - '%s'\n", cmd.argv[i]);
  }

	if (tiles_fp!=NULL)
	{
		printf("tiles created %d\n",sb_count(OPTS.tiles));		
		for (int q=0;q<sb_count(OPTS.tiles);q++)
		{
			tile_save(OPTS.tiles[q]);
			free(OPTS.tiles[q]);
		}
		fclose(tiles_fp);
	}

	if (map_fp!=NULL)
	{
		fwrite(OPTS.map,OPTS.map_w*OPTS.map_h*sizeof(uint16_t),1,map_fp);
		fclose(map_fp);
	}

	if (clut_fp!=NULL)
	{
		clut_save(&OPTS.bitmap);
		fclose(clut_fp);
	}

	if (meta_fp!=NULL)
	{
		printf("meta tiles created %d\n",sb_count(OPTS.metatiles));		
		for (int q=0;q<sb_count(OPTS.metatiles);q++)
		{
			fwrite(&OPTS.metatiles[q]->data,OPTS.meta_tile_x*OPTS.meta_tile_y*sizeof(uint16_t),1,meta_fp);
/*			
			int z=0;
			for (int y=0;y<OPTS.meta_tile_y;y++)
			{
				for (int x=0;x<OPTS.meta_tile_x;x++)
				{
					printf("%04x",OPTS.metatiles[q]->data[z++]);
				}
				printf("\n");
			}
			printf("\n");
*/			
//			tile_save(OPTS.tiles[q]);
			free(OPTS.metatiles[q]);
		}
		fclose(meta_fp);
	}
	image_show();
//	map_show();
	freeimage(&OPTS.bitmap);
/*
	for (int k=0;k<256;k++)
	{
		for (int i=0;i<256;i++)
		{
			for (int j=0;j<256;j++)
			{
				uint32_t code = (i*16)+j;
				printf("\x1b[38;2;%d;%d;%dm 1234", i, j, k);
			}
			printf("\n");
		}
	}
*/	
  command_free(&cmd);
  return 0;
}
#endif
