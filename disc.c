#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

//Copyright (c) 2019 MonstersGoBoom
//disc access

void save(const char *fname,uint8_t *ptr,uint32_t size)
{
	FILE *fp=fopen(fname,"wb");
	if (fp==NULL)
	{
		printf("file open failed %s\n",fname);
		return;
	}
	fwrite(ptr,size,1,fp);
	fclose(fp);
}

uint8_t *load(const char *fname,uint32_t *size)
{
FILE *fp=fopen(fname,"rb");
uint32_t len;
uint8_t *buffer;

	if (fp==NULL)
	{
		printf("%s failed to load\n",fname);
		return NULL;
	}
	fseek(fp,0,SEEK_END);
	len = ftell(fp);
	fseek(fp,0,SEEK_SET);

	if (size!=NULL) *size=len;

	buffer=malloc(len);
	if (buffer==NULL)
	{
		printf("alloc failed\n");
		return NULL;
	}
	fread(buffer,len,1,fp);
	fclose(fp);
	printf("load %s\n",fname);
	return buffer;	
}