#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <conio.h>
#include <commander.h>
#include <image.h>
#include <disc.h>

#include "stretchy_buffer.h"


#define SWAP_UINT16(x) (((x) >> 8) | ((x) << 8))


//Copyright (c) 2019 MonstersGoBoom
// tilemizer main 

/*	tileformats 
	"genesis" 4bpp 
	"gba"			4bpp 
	"nes"			2bpp
	"sms"			4bpp ( gamegear , wonderswan color )
	"gb"			2bpp
	"snes3"		3bpp
	"snes"		4bpp
	"pce"			4bpp
	"vboy"		2bpp
	"ngpc"		2bpp
*/

//	map formats aren't supported yet 
//	just raw 16 bit if defined 
//#define OUT_MAP

#define TILE_W 8 
#define TILE_H 8

typedef struct 
{
	uint8_t palette:6;
	uint8_t xflip:1;
	uint8_t yflip:1;
} TILE_ATTRIB;

typedef struct 
{
	uint32_t 	crc;
	uint8_t 	clut;	//	index 
	uint8_t 	bytes[TILE_W*TILE_H];
} TILE;

typedef struct 
{
	uint32_t 	crc;
	uint8_t 	data[TILE_W*TILE_H];
	TILE_ATTRIB 	attrib_data[TILE_W*TILE_H];
} META_TILE;

FILE *tiles_fp;
FILE *map_fp;
FILE *meta_fp;
FILE *clut_fp;

static struct 
{
	uint8_t planes;
	uint16_t repeats_x:1;
	uint16_t repeats_y:1;
	uint16_t repeats:1;
	uint16_t saveMap:2;	//	0 no ,1 8 bit , 2 16 bit , 3 spare
	uint16_t asCoords:1;
	uint16_t saveClut:1;
	uint16_t saveTile:1;
	uint16_t saveAttr:1;
	uint16_t mapoffset;
	uint8_t meta_tile_x;
	uint8_t meta_tile_y;
	uint16_t map_w;
	uint16_t map_h;
	uint8_t bpp;
	uint8_t max_colors;
	image_t bitmap;
	uint16_t *map;
	TILE_ATTRIB *attributes;
	TILE **tiles;
	META_TILE **metatiles;
	char *outname;
} OPTS;


FILE *bopen(const char *fname)
{
FILE *fp;
char temp_name[256];

	sprintf(temp_name,"%s_%s.bin",OPTS.outname,fname);
	fp = fopen(temp_name,"wb");

	if (fp==NULL)
	{
		printf("%s failed to open\n",temp_name);
		return NULL;
	}
	return fp;
}


static void outname(command_t *self)
{
	OPTS.outname = strdup(self->arg);
//  printf("Output: %s\n", self->arg);
}


static void outcoords(command_t *self)
{
	printf("%s\n",self->arg);
	OPTS.asCoords=1;
}

static void outtiles(command_t *self)
{
	printf("%s\n",self->arg);
	OPTS.saveTile=1;
}


static void out_metatile_size(command_t *self)
{
char *isvalid = strchr(self->arg,'*');
	if (isvalid==NULL)
	{
		printf("format isn't X*Y\n");
		exit(0);
		return;
	}
	char *xarg=(char*)&self->arg[0];
	char *yarg=isvalid+1;
	*isvalid = 0;

	OPTS.meta_tile_x = atoi(xarg);
	OPTS.meta_tile_y = atoi(yarg);
	if ((OPTS.meta_tile_x<1) || (OPTS.meta_tile_y<1))
	{
		OPTS.meta_tile_x=0;
		OPTS.meta_tile_y=0;
		return;
	}
	if (OPTS.meta_tile_x>8) OPTS.meta_tile_x=8;
	if (OPTS.meta_tile_y>8) OPTS.meta_tile_y=8;
	printf("Meta Tiles %d by %d\n",OPTS.meta_tile_x,OPTS.meta_tile_y);
}

//	command line opt -m b <bytes> -m w <words>
static void out_attribs(command_t *self)
{
	OPTS.saveAttr = 1;
}
static void out_map(command_t *self)
{
	if (self->arg==NULL)
		OPTS.saveMap = 2;
	else 
	{
		if (strchr(self->arg,'b')!=NULL)
			OPTS.saveMap = 1;
		if (strchr(self->arg,'w')!=NULL)
			OPTS.saveMap = 2;
	}
}

//	command line opt -r xy select axis repeat checking 

static void check_repeats(command_t *self)
{
	if (self->arg!=NULL)
	{
		OPTS.repeats=1;
		OPTS.repeats_x=strchr(self->arg,'x')!=NULL;
		OPTS.repeats_y=strchr(self->arg,'y')!=NULL;
	}
}

//	command line opt -p 1,2,3,4 only supports up to 4 planes as output 

static void set_plane_format(command_t *self)
{
	uint8_t p = atoi(self->arg);
	if (p<1) p=1;
	if (p>4) p=4;
	OPTS.planes=p;
	printf("Planes set %d\n",OPTS.planes);
}

static void set_map_offset(command_t *self)
{
	uint16_t p = atoi(self->arg);
	OPTS.mapoffset = p;
	printf("map offset %d\n",p);
}

//	command line opt -b 1,2,3,4 bitplanes for input data 
//	say you have an image where your palettes
//  4 colors each ( NES ) image is Clut 0 0123 Clut 1 4567 etc. 
//  8 colors each ( ? ) image is Clut 0 01234567 Clut 1 89ABCDEF etc. 
//  16 colors each ( Genesis ) image is Clut 0 00010203-0F Clut 1 10111213-1F etc.  

static void set_max_colors(command_t *self)
{
	uint8_t p = atoi(self->arg);
	if (p<1) p=1;
	if (p>8) p=4;
	OPTS.bpp=p;
	OPTS.max_colors = 1<<(p);

	printf("BPP set %d %d\n",OPTS.bpp,OPTS.max_colors);
}

//	machine specific tile saves 
//	virtual boy 
static void tile_save_vboy(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>2;x++)
		{
			byte=(*ptr++&0x3);
			byte|=(*ptr++&0x3)<<2;
			byte|=(*ptr++&0x3)<<4;
			byte|=(*ptr++&0x3)<<6;
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}
//	machine specific tile saves 
//	neo geo pocket color
static void tile_save_ngpc(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)+TILE_W-1];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>2;x++)
		{
			byte=(*ptr--&0x3);
			byte|=(*ptr--&0x3)<<2;
			byte|=(*ptr--&0x3)<<4;
			byte|=(*ptr--&0x3)<<6;
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}
//	machine specific tile saves 
//	Sega Genesis 
static void tile_save_genesis(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>1;x++)
		{
			byte=(*ptr++&0xf)<<4;
			byte|=(*ptr++&0xf);
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}
//	machine specific tile saves 
//	Gameboy Advance
static void tile_save_gba(TILE *tile)
{
	for (int _y=0;_y<TILE_H;_y++)
	{
		uint8_t *ptr=&tile->bytes[(_y*TILE_W)];
		uint8_t byte=0;
		for (int x=0;x<TILE_W>>1;x++)
		{
			byte=(*ptr++&0xf);
			byte|=(*ptr++&0xf)<<4;
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}

//	machine specific tile saves 
//	SNES , PC Engine
static void tile_save_snes_planes(TILE *tile)
{
	for (int y=0;y<TILE_H;y++)
	{
		for (int plane=0;plane<2;plane++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
	for (int y=0;y<TILE_H;y++)
	{
		for (int plane=2;plane<OPTS.planes;plane++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}

//	none specific tile saves 
//	as bitplanes 
static void tile_save_planes(TILE *tile)
{
	for (int plane=0;plane<OPTS.planes;plane++)
	{
		for (int y=0;y<TILE_H;y++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}

//	none specific tile saves 
//	as interleaved bitplanes 
static void tile_save_interleaved_planes(TILE *tile)
{
	for (int y=0;y<TILE_H;y++)
	{
		for (int plane=0;plane<OPTS.planes;plane++)
		{
			uint8_t byte=0;
			for (int x=0;x<TILE_W;x++)
			{
				if ( ((tile->bytes[x+(y*TILE_W)]>>plane)&1) !=0)
				{
					byte|=1<<(7-x);
				}	
			}
			fwrite(&byte,1,1,tiles_fp);
		}
	}
}


//	none specific tile saves 
//	as interleaved bitplanes 
static void tile_save_collider(TILE *tile)
{
	for (int x=0;x<TILE_W;x++)
	{
		uint8_t h=0;
		uint8_t col;
		for (int y=0;y<TILE_H;y++)
		{
			if (tile->bytes[x+(y*TILE_W)]!=0)
			{
				h=8-y;
				col = tile->bytes[x+(y*TILE_W)];
				break;
			}	
		}
		fwrite(&h,1,1,tiles_fp);
		fwrite(&col,1,1,tiles_fp);
	}
}


static void clut_save_RGB888(image_t *image)
{
	for (int q=0;q<256;q++)
	{
		uint8_t r=image->colors[(q*4)+2];
		uint8_t g=image->colors[(q*4)+1];
		uint8_t b=image->colors[(q*4)+0];
		fwrite(&r,1,1,clut_fp);
		fwrite(&g,1,1,clut_fp);
		fwrite(&b,1,1,clut_fp);
	}
}

static void clut_save_Genesis(image_t *image)
{
	for (int q=0;q<256;q++)
	{
		uint16_t rgb;
		uint8_t r=image->colors[(q*4)+2];
		uint8_t g=image->colors[(q*4)+1];
		uint8_t b=image->colors[(q*4)+0];

		rgb= (b>>4);
		rgb|=(r>>4)<<8;
		rgb|=(g>>4)<<12;
		fwrite(&rgb,2,1,clut_fp);
	}
}

static void clut_save_PCE(image_t *image)
{
	for (int q=0;q<256;q++)
	{
		uint16_t rgb;
		uint8_t r=image->colors[(q*4)+2];
		uint8_t g=image->colors[(q*4)+1];
		uint8_t b=image->colors[(q*4)+0];

		rgb= (b>>5);
		rgb|=(r>>5)<<3;
		rgb|=(g>>5)<<6;
		fwrite(&rgb,2,1,clut_fp);
	}
}

static void save_map_block()
{
	for (int y=0;y<OPTS.map_h;y++)
	{
		for (int x=0;x<OPTS.map_w;x++)
		{
			uint16_t blank=(OPTS.map[x+(y*OPTS.map_w)]);
			TILE_ATTRIB *attr = (&OPTS.attributes[x+(y*OPTS.map_w)]);
			blank+=OPTS.mapoffset;
			fwrite(&blank,1,2,map_fp);
		}
	}
}

static void save_map_block_PCE()
{
	for (int y=0;y<OPTS.map_h;y++)
	{
		for (int x=0;x<OPTS.map_w;x++)
		{
			uint16_t blank=(OPTS.map[x+(y*OPTS.map_w)]);
			TILE_ATTRIB *attr = (&OPTS.attributes[x+(y*OPTS.map_w)]);
			blank+=OPTS.mapoffset;
			blank |= attr->palette<<12;
//						blank = SWAP_UINT16(blank);
			fwrite(&blank,1,2,map_fp);
		}
	}
}

static void (*tile_save)(TILE *tile)=tile_save_planes;
static void (*clut_save)(image_t *image)=clut_save_RGB888;
static void (*map_save)()=save_map_block;

typedef struct 
{
	const char *name;
	void ( *save)(TILE *tile); 
	void ( *saveclut)(image_t *tile); 
	void ( *savemaptile)(); 
	uint8_t planes;
} TYPE_T;
	
TYPE_T types[]=
{
	{"genesis",tile_save_genesis,clut_save_Genesis,save_map_block,4},
	{"gba",tile_save_gba,clut_save_RGB888,save_map_block,4},
	{"nes",tile_save_planes,clut_save_RGB888,save_map_block,2},
	{"sms",tile_save_interleaved_planes,clut_save_RGB888,save_map_block,4},
	{"gb",tile_save_interleaved_planes,clut_save_RGB888,save_map_block,2},
	{"snes3",tile_save_snes_planes,clut_save_RGB888,save_map_block,3},
	{"snes",tile_save_snes_planes,clut_save_RGB888,save_map_block,4},
	{"pce",tile_save_snes_planes,clut_save_PCE,save_map_block_PCE,4},
	{"vboy",tile_save_vboy,clut_save_RGB888,save_map_block,2},
	{"ngpc",tile_save_ngpc,clut_save_RGB888,save_map_block,2},
	{"jet",tile_save_planes,clut_save_Genesis,save_map_block,3},
	{"collider",tile_save_collider,clut_save_RGB888,save_map_block,4},
	{NULL,NULL,NULL,0},
};

static void set_machine(command_t *self)
{
	for (int q=0;types[q].name!=NULL;q++)
	{
		if (stricmp(self->arg,types[q].name)==0)
		{
			printf("%s\n",types[q].name);
			tile_save = types[q].save;
			clut_save = types[q].saveclut;
			map_save = types[q].savemaptile;
			OPTS.planes = types[q].planes;
			OPTS.saveClut = 1;
			return;
		}
	}
}

static void tile_grab(int x,int y,uint16_t *map,TILE_ATTRIB *attrib)
{
TILE b;
	b.crc = 0;
	b.clut = 0;
	for (int _y=0;_y<TILE_H;_y++)
	{
		for (int _x=0;_x<TILE_W;_x++)
		{
			uint8_t byte=OPTS.bitmap.buffer[(x+_x) + (((OPTS.bitmap.height-1)-(y+_y))*OPTS.bitmap.width)];	//	only bottom 4 bits 
			uint8_t clut= (byte/OPTS.max_colors)&0xf;
			if (((byte%OPTS.max_colors)!=0) && (b.clut==0))
				b.clut=clut;
			b.crc^=byte%OPTS.max_colors;
			b.bytes[_x+(_y*TILE_W)]=byte%OPTS.max_colors;
		}
	}
	uint16_t ret=sb_count(OPTS.tiles);
	
	if ((OPTS.repeats_x==1) || (OPTS.repeats_y==1) || (OPTS.repeats==1))
	{
		for (uint16_t q=0;q<ret;q++)
		{
			//	good chance of match
			if (OPTS.tiles[q]->crc==b.crc)
			{
				int mdiffer = 0;

				//	check fully to be sure 
				for (int _x=0;_x<TILE_W*TILE_H;_x++)
					if (OPTS.tiles[q]->bytes[_x]!=b.bytes[_x])
						mdiffer++;

				//	nope it's exactly the same , but the clut could be different 
				if (mdiffer==0)			
				{
					*map = q;
					attrib->xflip=0;
					attrib->yflip=0;
					attrib->palette=b.clut;
					return;
				}	
				else 
				{
					//	check xflip
					if (OPTS.repeats_x==1)
					{
						mdiffer = 0;
						for (int _y=0;_y<TILE_H;_y++)
							for (int _x=0;_x<TILE_W;_x++)
								if (OPTS.tiles[q]->bytes[_x+(_y*TILE_W)]!=b.bytes[(TILE_W-1-_x)+(_y*TILE_W)])
									mdiffer++;

						if (mdiffer==0)			
						{
//							uint16_t ret=q&0x7ff;
//						ret|=b.clut<<13;
//						ret|=1<<11;
							*map = q;
							attrib->palette=b.clut;
							attrib->xflip=1;
	
							return;
						}
					}

					if (OPTS.repeats_x==1)
					{
						//	check yflip
						mdiffer = 0;
						for (int _y=0;_y<TILE_H;_y++)
							for (int _x=0;_x<TILE_W;_x++)
								if (OPTS.tiles[q]->bytes[_x+(_y*TILE_W)]!=b.bytes[_x+((TILE_H-1-_y)*TILE_W)])
									mdiffer++;

						if (mdiffer==0)			
						{
//							uint16_t ret=q&0x7ff;
							*map = q;
							attrib->palette=b.clut;
							attrib->yflip=1;

//							ret|=b.clut<<13;
	//						ret|=1<<12;
							return;
						}

						if (OPTS.repeats_x==1)
						{
						//	check yflip & xflip
							mdiffer = 0;
							for (int _y=0;_y<TILE_H;_y++)
								for (int _x=0;_x<TILE_W;_x++)
									if (OPTS.tiles[q]->bytes[_x+(_y*TILE_W)]!=b.bytes[(TILE_W-1-_x)+((TILE_H-1-_y)*TILE_W)])
										mdiffer++;

							if (mdiffer==0)			
							{
//								uint16_t ret=q&0x7ff;
								*map = q;
								attrib->palette=b.clut;
								attrib->xflip=1;
								attrib->yflip=1;

//								ret|=b.clut<<13;
	//							ret|=1<<11;
		//						ret|=1<<12;
								return;
							}
						}
					}
				}
			}
		}
	}

	TILE *copy = malloc(sizeof(TILE));
	memcpy(copy,&b,sizeof(TILE));
	sb_push(OPTS.tiles,copy);
//	ret&=0x7ff;
//	ret|=b.clut<<12;
	*map=ret;
	attrib->palette=b.clut;
	attrib->xflip=0;
	attrib->yflip=0;
//	return ret;
}
static uint16_t metatile_grab(int x,int y)
{
	uint16_t ret;
	META_TILE mt;
	mt.crc = 0;

	for (int _y=0;_y<OPTS.meta_tile_y;_y++)
	{
		for (int _x=0;_x<OPTS.meta_tile_x;_x++)
		{
			uint16_t t;
			TILE_ATTRIB a;
			tile_grab((x+_x)*TILE_W,(y+_y)*TILE_H,&t,&a);
			mt.crc^=t;
			mt.crc^=a.palette;
			mt.data[_x+(_y*OPTS.meta_tile_x)]=t;
			mt.attrib_data[_x+(_y*OPTS.meta_tile_x)].palette=a.palette;
			mt.attrib_data[_x+(_y*OPTS.meta_tile_x)].xflip=a.xflip;
			mt.attrib_data[_x+(_y*OPTS.meta_tile_x)].yflip=a.yflip;
		}
	}
	//	we have a tile so check for repeats 
	ret=sb_count(OPTS.metatiles);
	for (uint16_t q=0;q<ret;q++)
	{
		//	good chance of match
		if (OPTS.metatiles[q]->crc==mt.crc)
		{
			int mdiffer = 0;
			//	check fully to be sure 
			for (int _x=0;_x<OPTS.meta_tile_x*OPTS.meta_tile_y;_x++)
				if (OPTS.metatiles[q]->data[_x]!=mt.data[_x])
					mdiffer++;
			for (int _x=0;_x<OPTS.meta_tile_x*OPTS.meta_tile_y;_x++)
				if (OPTS.metatiles[q]->attrib_data[_x].palette!=mt.attrib_data[_x].palette)
					mdiffer++;

			if (mdiffer==0)			
			{
				ret=q;
				return ret;
			}	
		}
	}
	META_TILE *copy = malloc(sizeof(META_TILE));
	memcpy(copy,&mt,sizeof(META_TILE));
	sb_push(OPTS.metatiles,copy);
	return ret;
}

int main(int argc, char **argv)
{
  command_t cmd;
	OPTS.mapoffset = 0;
	OPTS.planes = 0;	
	OPTS.repeats = 0;
	OPTS.repeats_x = 0;
	OPTS.repeats_y = 0;
	OPTS.saveMap = 0;
	OPTS.saveAttr = 0;
	OPTS.saveClut = 0;	
	OPTS.saveTile = 0;
	OPTS.asCoords = 0;
	OPTS.tiles = NULL;
	OPTS.metatiles = NULL;
	OPTS.max_colors = 16;
	OPTS.bpp = 4;
	OPTS.meta_tile_x = 0;
	OPTS.meta_tile_y = 0;
  command_init(&cmd, argv[0], "0.1.0");
  command_option(&cmd, "-o", "--Output <arg>", "Output filename", outname);
	command_option(&cmd, "-p", "--Planes <arg>", "Number Of Planes -p 1-4", set_plane_format);
	command_option(&cmd, "-O", "--Offset <arg>", "map offset", set_map_offset);
	command_option(&cmd, "-r", "--Repeats [arg]", "Check for repeats (-r xy) (-r x) (-r y)", check_repeats);

	command_option(&cmd, "-m", "--map <args>", "Output Map [ XxY b=byte,w=word ]", out_map);
	command_option(&cmd, "-a", "--attribs", "", out_attribs);
	command_option(&cmd, "-t", "--metaTile <args>", "8x8 metatile size", out_metatile_size);
	command_option(&cmd, "-T", "--write tiles", "write tiles", outtiles);
	command_option(&cmd, "-c", "--Coordinates", "as xpos,ypos,tileid", outcoords);
	command_option(&cmd, "-b", "--Bits Per Pixel per tile <arg>", "#max 1,2,3,4", set_max_colors);
	command_option(&cmd, "-f", "--machine format <arg>", "genesis,nes,gba,ngpc,vb,snes4,snes3,pce", set_machine);

  command_parse(&cmd, argc, argv);

	printf("X repeats %d:",OPTS.repeats_x);
	printf("Y repeats %d\n",OPTS.repeats_y);
	printf("Map Output %d\n",OPTS.saveMap);
	printf("Colors Per Tile %d\n",OPTS.max_colors);
	printf("TileSize %dx%d\n",TILE_W,TILE_H);
	printf("MetaTile %dx%d\n",OPTS.meta_tile_x,OPTS.meta_tile_y);

	if (OPTS.saveTile==1)
		tiles_fp = bopen("tiles");
	else
		tiles_fp = NULL;	

  for (int i = 0; i < cmd.argc; ++i)
	{
		char *isbmp = strstr(cmd.argv[i],".bmp");
		if (isbmp!=NULL)
		{
			loadimage(cmd.argv[i],&OPTS.bitmap);
			OPTS.map_w = OPTS.bitmap.width / TILE_W;
			OPTS.map_h = OPTS.bitmap.height / TILE_H;
			printf("Map %dx%d\n",OPTS.map_w,OPTS.map_h);
			//	we always make a 16 bit map 
			//	how we output is later 
			if ((OPTS.meta_tile_x==0) || (OPTS.meta_tile_y==0))
			{
				printf("no meta\n");
				OPTS.map = (uint16_t*)malloc(OPTS.map_w*OPTS.map_h*sizeof(uint16_t));
				OPTS.attributes = (TILE_ATTRIB*)malloc(OPTS.map_w*OPTS.map_h*sizeof(TILE_ATTRIB));
				for (int y=0;y<OPTS.map_h;y++)
				{
					for (int x=0;x<OPTS.map_w;x++)
					{
						tile_grab(x*TILE_W,y*TILE_H,&OPTS.map[x+(y*OPTS.map_w)],&OPTS.attributes[x+(y*OPTS.map_w)]);
//						OPTS.map[x+(y*OPTS.map_w)]=SWAP_UINT16(re);
					}
				}
			}
			else
			{
				uint16_t mw,mh;
				mw = OPTS.map_w/OPTS.meta_tile_x;
				mh = OPTS.map_h/OPTS.meta_tile_y;
				OPTS.map = (uint16_t*)malloc(mw*mh*sizeof(uint16_t));
				OPTS.attributes = (TILE_ATTRIB*)malloc(mw*mh*sizeof(TILE_ATTRIB));
				printf("Map %dx%d\n",mw,mh);
				printf("meta\n");

				//	map in segments
				for (int y=0;y<mh;y++)
				{
					for (int x=0;x<mw;x++)
					{
						OPTS.map[x+(y*mw)]=metatile_grab(x*OPTS.meta_tile_x,y*OPTS.meta_tile_y);
					}
				}
				OPTS.map_w = mw;
				OPTS.map_h = mh;
			}
		}
    printf("  - '%s'\n", cmd.argv[i]);
  }

	if (tiles_fp!=NULL)
	{
		printf("tiles created %d\n",sb_count(OPTS.tiles));		
		for (int q=0;q<sb_count(OPTS.tiles);q++)
		{
			tile_save(OPTS.tiles[q]);
			free(OPTS.tiles[q]);
		}
		fclose(tiles_fp);
	}

	if (OPTS.saveMap!=0)
	{
		map_fp = bopen("map");
		if (map_fp!=NULL)
		{
			if (OPTS.saveMap==1)
			{
				for (int y=0;y<OPTS.map_h;y++)
				{
					for (int x=0;x<OPTS.map_w;x++)
					{
						uint16_t blank=OPTS.map[x+(y*OPTS.map_w)]&0xff;
						fwrite(&blank,1,1,map_fp);
					}
				}
			}
			else 
			{
				map_save();
			}
//				fwrite(OPTS.map,OPTS.map_w*OPTS.map_h*sizeof(uint16_t),1,map_fp);

			fclose(map_fp);
		}
	}

	if (OPTS.saveAttr!=0)
	{
		map_fp = bopen("attr");
		if (map_fp!=NULL)
		{
			for (int y=0;y<OPTS.map_h;y++)
			{
				for (int x=0;x<OPTS.map_w;x++)
				{
					fwrite(&OPTS.attributes[x+(y*OPTS.map_w)],1,1,map_fp);
				}
			}

			fclose(map_fp);
		}
	}

	if (OPTS.asCoords!=0)
	{
		map_fp = bopen("xy");
		if (map_fp!=NULL)
		{
			for (int y=1;y<OPTS.map_h;y++)
			{
				for (int x=0;x<OPTS.map_w;x++)
				{
					uint16_t blank=SWAP_UINT16(OPTS.map[x+(y*OPTS.map_w)]&0x7ff);

					if (blank!=0)
					{
						printf("id %x\n",blank);
						blank=SWAP_UINT16(blank);
						fwrite(&blank,2,1,map_fp);
						blank=SWAP_UINT16(x);
						printf("x %x %x\n",x,blank);
						fwrite(&blank,2,1,map_fp);
						blank=SWAP_UINT16(y);
						printf("y %x %x\n",y,blank);
						fwrite(&blank,2,1,map_fp);
						blank=0;
						fwrite(&blank,2,1,map_fp);

					}
				}
			}
			fclose(map_fp);
		}
	}

	if (OPTS.saveClut==1)
	{
		clut_fp = bopen("clut");
		if (clut_fp!=NULL)
		{
			clut_save(&OPTS.bitmap);
			fclose(clut_fp);
		}
	}
	if ((OPTS.meta_tile_x!=0) && (OPTS.meta_tile_y!=0))
	{
		meta_fp = bopen("meta");
		if (meta_fp!=NULL)
		{
			printf("meta tiles created %d\n",sb_count(OPTS.metatiles));		
			for (int q=0;q<sb_count(OPTS.metatiles);q++)
			{
//				fwrite(&OPTS.metatiles[q]->data,OPTS.meta_tile_x*OPTS.meta_tile_y*sizeof(uint16_t),1,meta_fp);
				fwrite(&OPTS.metatiles[q]->data,OPTS.meta_tile_x*OPTS.meta_tile_y*sizeof(uint8_t),1,meta_fp);
				fwrite(&OPTS.metatiles[q]->attrib_data,OPTS.meta_tile_x*OPTS.meta_tile_y*sizeof(uint8_t),1,meta_fp);
				free(OPTS.metatiles[q]);
			}
			fclose(meta_fp);
		}

	}
	freeimage(&OPTS.bitmap);
  command_free(&cmd);
  return 0;
}
