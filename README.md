# Tilemizer

λ tilemizer.exe -h

  Usage: tilemizer.exe [options]

  Options:

    -V, --version                 output program version
    -h, --help                    output help information
    -o, --Output <arg>            Output filename
    -p, --Planes <arg>            Number Of Planes -p 1-4
    -r, --Repeats [arg]           Check for repeats (-r xy) (-r x) (-r y)
    -b, --Bits Per Pixel per tile <arg> #max 1,2,3,4
    -f, --machine format <arg>    genesis,nes,gba,ngpc,vb,snes4,snes3,pce


EG
```
tilemizer.exe -r x -m w -b 2 -f nes demo.bmp
```
will output nes format tiles 

* 	"genesis"   4bpp 
* 	"gba"	    4bpp 
* 	"nes"		2bpp
* 	"sms"		4bpp ( gamegear , wonderswan color )
* 	"gb"		2bpp
* 	"snes3"		3bpp
* 	"snes"		4bpp
* 	"pce"		4bpp
* 	"vboy"		2bpp
* 	"ngpc"		2bpp
