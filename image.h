// Copyright (c) 2019 MonstersGoBoom
// bmp / image utils 

typedef struct 
{
	uint8_t *buffer;
	uint32_t width;
	uint32_t height;
	uint8_t *colors;
} image_t;

void freeimage(image_t *image);
int loadimage(const char *fname,image_t *image);


