CC = gcc

tilemizer: tilemizer.c image.c disc.c commander/src/commander.c
	$(CC) $^ -o $@ -Wall -Os -s -Wextra -I commander/src -I .

clean:
	rm -f tilemizer

.PHONY: clean
